import React from 'react';
import styles from './styles.module.css';

function Card({ productItem, onAddToCart = () => {} }) {
  return (
    <div className={styles.container}>
      <div className='card'>
        <div className='card-image'>
          <img src={productItem.img} alt={productItem.title} />
          <span className='card-title'>{productItem.title}</span>
          <span
            to='/'
            className='btn-floating halfway-fab waves-effect waves-light red'
            onClick={onAddToCart}>
            <i className='material-icons'>add</i>
          </span>
        </div>

        <div className='card-content'>
          <p>{productItem.desc}</p>
          <p>
            <b>
              Price:
              {productItem.price}$
            </b>
          </p>
        </div>
      </div>
    </div>
  );
}

export default Card;
