import { useCallback } from 'react';
import { connect } from 'react-redux';

import { addToCart } from '../../redux/actions/cartActions';

import Card from '../../components/Card';

import styles from './styles.module.css';

const Home = ({ items, addToCart }) => {
  const handleAddToCart = useCallback(
    (product) => () => {
      addToCart(product.id);
    },
    [addToCart],
  );

  return (
    <div className='container'>
      <h3 className='center'>Our items</h3>
      <div className={styles.card_list}>
        {items.map((item) => (
          <Card key={item.id} productItem={item} onAddToCart={handleAddToCart(item)} />
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  items: state.items,
});
const mapDispatchToProps = (dispatch) => ({
  addToCart: (id) => {
    dispatch(addToCart(id));
  },
});

Home.defaultProps = {
  items: [],
  addToCart: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
