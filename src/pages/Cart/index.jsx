import { useCallback } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FaPlus, FaMinus, FaTrash } from 'react-icons/fa';

import { addQuantity, removeItem, subtractQuantity } from '../../redux/actions/cartActions';

import styles from './styles.module.css';

const Cart = ({ items, total, removeItem, addQuantity, subtractQuantity }) => {
  const handleRemove = useCallback(
    (id) => {
      removeItem(id);
    },
    [removeItem],
  );
  const handleAddQuantity = useCallback(
    (id) => {
      addQuantity(id);
    },
    [addQuantity],
  );
  const handleSubtractQuantity = useCallback(
    (id) => {
      subtractQuantity(id);
    },
    [subtractQuantity],
  );

  return (
    <div className='container'>
      <div className='cart'>
        <h5>You have ordered:</h5>
        {items.length ? (
          <>
            <ul className={styles.product_list}>
              {items.map((item) => (
                <li className={styles.product_list_item} key={item.id}>
                  <div className={styles.product_list_item_image}>
                    <img src={item.img} alt={item.img} />
                  </div>
                  <div className={styles.product_list_item_description}>
                    <span className={styles.product_list_item_description_title}>{item.title}</span>
                    <span>{item.desc}</span>
                  </div>
                  <div className={styles.product_list_item_price}>
                    <b>
                      Price:
                      {item.price}$
                    </b>
                  </div>

                  <div className={styles.product_list_item_actions}>
                    <div className={styles.product_list_item_quantity}>
                      <Link to='/cart'>
                        <FaPlus
                          onClick={() => {
                            handleAddQuantity(item.id);
                          }}
                        />
                      </Link>
                      <span className={styles.quantity}>
                        Count:
                        {item.quantity}
                      </span>
                      <Link to='/cart'>
                        <FaMinus
                          onClick={() => {
                            handleSubtractQuantity(item.id);
                          }}
                        />
                      </Link>
                    </div>
                    <button
                      className='waves-effect waves-light btn red'
                      onClick={() => {
                        handleRemove(item.id);
                      }}>
                      <FaTrash /> Remove item
                    </button>
                  </div>
                </li>
              ))}
            </ul>
            <div className={styles.product_list_total}>
              Total price: <b>${total}</b>
            </div>
          </>
        ) : (
          <p>Looks like nothing here</p>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  items: state.addedItems,
  total: state.total,
});
const mapDispatchToProps = (dispatch) => ({
  removeItem: (id) => {
    dispatch(removeItem(id));
  },
  addQuantity: (id) => {
    dispatch(addQuantity(id));
  },
  subtractQuantity: (id) => {
    dispatch(subtractQuantity(id));
  },
});

Cart.defaultProps = {
  items: [],
  total: 0,
  removeItem: () => {},
  addQuantity: () => {},
  subtractQuantity: () => {},
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
