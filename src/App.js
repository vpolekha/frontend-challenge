import React from 'react';
import { Route, Switch } from 'react-router-dom';

import Navbar from './components/Navbar';

import Home from './pages/Home';
import Cart from './pages/Cart';
import Error from './pages/404';

export const App = () => (
  <div className='App'>
    <Navbar />
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/cart' component={Cart} />
      <Route path='*' component={Error} />
    </Switch>
  </div>
);

export default App;
