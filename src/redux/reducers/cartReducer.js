import {
  ADD_QUANTITY,
  ADD_TO_CART,
  REMOVE_ITEM,
  SUB_QUANTITY,
} from '../actions/action-types/cart-actions';

import { mockItems } from '../../assets/mockData';

const initState = {
  items: mockItems,
  addedItems: [],
  total: 0,
};
const cartReducer = (state = initState, action) => {
  if (action.type === ADD_TO_CART) {
    const addedItem = state.items.find((item) => item.id === action.id);
    const existedItem = state.addedItems.find((item) => action.id === item.id);
    if (existedItem) {
      addedItem.quantity += 1;
      return {
        ...state,
        total: state.total + addedItem.price,
      };
    }

    addedItem.quantity = 1;
    const total = state.total + addedItem.price;

    return {
      ...state,
      addedItems: [...state.addedItems, addedItem],
      total,
    };
  }
  if (action.type === REMOVE_ITEM) {
    const removedItem = state.addedItems.find((item) => action.id === item.id);
    const newItems = state.addedItems.filter((item) => action.id !== item.id);

    const total = state.total - removedItem.price * removedItem.quantity;
    return {
      ...state,
      addedItems: newItems,
      total,
    };
  }
  if (action.type === ADD_QUANTITY) {
    const addedItem = state.items.find((item) => item.id === action.id);
    ++addedItem.quantity;
    const total = state.total + addedItem.price;
    return {
      ...state,
      total,
    };
  }
  if (action.type === SUB_QUANTITY) {
    const addedItem = state.items.find((item) => item.id === action.id);
    if (addedItem.quantity === 1) {
      const newItems = state.addedItems.filter((item) => item.id !== action.id);
      const total = state.total - addedItem.price;
      return {
        ...state,
        addedItems: newItems,
        total,
      };
    }

    --addedItem.quantity;
    const total = state.total - addedItem.price;
    return {
      ...state,
      total,
    };
  }

  return state;
};

export default cartReducer;
